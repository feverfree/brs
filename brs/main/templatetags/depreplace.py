from django import template

register = template.Library()

@register.filter
def depreplace(value):
    return value.replace(",",", ")
