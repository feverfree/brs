from django.db import models

# Create your models here.
class brsinfo (models.Model):
    system_id = models.AutoField(primary_key=True)
    system_name = models.CharField(max_length=50)
    system_status = models.IntegerField(default=0)
    status_infra_user = models.CharField(max_length=20, null=True, blank=True)
    status_ba_user = models.CharField(max_length=20, null=True, blank=True)
    status_system_start_time = models.DateTimeField(null=True, blank=True)
    status_system_end_time = models.DateTimeField(null=True, blank=True)
    status_check_end_time = models.DateTimeField(null=True, blank=True)
    system_rto = models.CharField(max_length=20, null=True, blank=True)
    status_ap_user = models.CharField(max_length=20, null=True, blank=True)
    status_ap_end_time = models.DateTimeField(null=True, blank=True)
    tokeninfo = models.CharField(null=True, max_length=100)

    class Meta:
        db_table = "brsinfo"

class systemdepend (models.Model):
    system_id = models.IntegerField(null=True)
    depend = models.IntegerField(null=True)

    class Meta:
        db_table = "systemdepend"
