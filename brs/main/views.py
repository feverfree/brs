from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic import TemplateView
from datetime import datetime
from time import gmtime
from time import strftime
import requests
import dateutil
from dateutil.parser import parse
from main.models import brsinfo
from xlwt import Workbook
from django.http import HttpResponse
from io import StringIO,BytesIO

# Create your views here.
class brsstatus(TemplateView):
    template_name = "brsstatus.html"

    def get_context_data(self, **kwargs):
        mainsql='select a.system_id,a.system_name,a.status_ba_user,a.status_check_end_time,a.status_infra_user,a.status_system_end_time,a.status_system_start_time,a.system_status,a.system_rto,a.status_ap_user,a.status_ap_end_time,b.depend_name from brsinfo a left join (select a.system_id,group_concat(b.system_name) "depend_name" from systemdepend a join brsinfo b on a.depend=b.system_id and b.system_status < 3 group by a.system_id) b on a.system_id=b.system_id'
        context = super().get_context_data(**kwargs)
        #context['db'] = brsinfo.objects.all()
        context['db'] = brsinfo.objects.raw(mainsql)
        return context

class reportpage(TemplateView):
    template_name = "reportpage.html"

    def get_context_data(self, **kwargs):
        mainsql='select a.system_id,a.system_name,a.status_ba_user,a.status_check_end_time,a.status_infra_user,a.status_system_end_time,a.status_system_start_time,a.system_status,a.system_rto,a.status_ap_user,a.status_ap_end_time,b.depend_name from brsinfo a left join (select a.system_id,group_concat(b.system_name) "depend_name" from systemdepend a join brsinfo b on a.depend=b.system_id and b.system_status < 3 group by a.system_id) b on a.system_id=b.system_id'
        context = super().get_context_data(**kwargs)
        context['db'] = brsinfo.objects.raw(mainsql)
        return context

class inputpage(TemplateView):
    template_name = "inputpage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['db'] = brsinfo.objects.filter(system_id=self.request.GET['token'])[0]
        except:
            pass
        return context

class update_status(View):

    def post(self, request):
        try:
            input_id = int(request.POST['system_id'])
            inputtype = int(request.POST['intype'])
            username = request.POST['username']
            try:
                handtime = dateutil.parser.parse(request.POST['handtime'])
            except:
                handtime = None
            if inputtype == 1:
                query = brsinfo.objects.filter(system_id=input_id)[0]
                query.system_status = 1
                query.status_infra_user = username
                if handtime is None:
                    query.status_system_start_time = datetime.now()
                else:
                    query.status_system_start_time = handtime
                query.save()
                msg = '【'+datetime.strftime(query.status_system_start_time, '%H:%M') + '】' + query.status_infra_user + '正在開啟' + query.system_name
            elif inputtype == 2:
                query = brsinfo.objects.filter(system_id=input_id)[0]
                query.system_status = 2
                query.status_infra_user = username
                if handtime is None:
                    query.status_system_end_time = datetime.now()
                else:
                    query.status_system_end_time = handtime
                #query.system_rto = round((( query.status_system_end_time - query.status_system_start_time).total_seconds() ) / 60,2)
                #query.system_rto = strftime("%H:%M:%S", gmtime(( query.status_system_end_time - query.status_system_start_time).total_seconds()))
                query.save()
                tokenbysys = query.tokeninfo
                msg = '【'+datetime.strftime(query.status_system_end_time, '%H:%M') + '】' + query.status_infra_user + '完成開啟' + query.system_name
            elif inputtype == 3:
                query = brsinfo.objects.filter(system_id=input_id)[0]
                query.system_status = 3
                query.status_ap_user = username
                if handtime is None:
                    query.status_ap_end_time = datetime.now()
                else:
                    query.status_ap_end_time = handtime
                query.system_rto = strftime("%H:%M:%S", gmtime(( query.status_ap_end_time - query.status_system_start_time).total_seconds()))
                query.save()
                tokenbysys = query.tokeninfo
                msg = '【'+datetime.strftime(query.status_ap_end_time, '%H:%M') + '】' + query.status_ap_user + '完成AP驗證' + query.system_name
            elif inputtype == 4:
                query = brsinfo.objects.filter(system_id=input_id)[0]
                query.system_status = 4
                query.status_ba_user = username
                if handtime is None:
                    query.status_check_end_time = datetime.now()
                else:
                    query.status_check_end_time = handtime
                query.save()
                tokenbysys = query.tokeninfo
                msg = '【'+datetime.strftime(query.status_check_end_time, '%H:%M') + '】' + query.status_ba_user + '完成業管驗證' + query.system_name
            print(msg)
            tokenbysys = query.tokeninfo
            #tokencenter = 'IbgIIgmjDjhsURzdWdv2kmVXv8zFwlVmKdxM9uYxv4d'
            pushmsg = {'message':msg}
            headersbysys = {"Authorization": "Bearer " + tokenbysys,"Content-Type": "application/x-www-form-urlencoded"}
            r = requests.post("https://notify-api.line.me/api/notify", headers=headersbysys, params=pushmsg)
            #headers = {"Authorization": "Bearer " + tokencenter,"Content-Type": "application/x-www-form-urlencoded"}
            #r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=pushmsg)

        except Exception as e:
            print(e)

        return redirect('/brsstatus/')  # Return to Home

def exportpage(request):
    mainsql='select a.system_id,a.system_name,a.status_ba_user,a.status_check_end_time,a.status_infra_user,a.status_system_end_time,a.status_system_start_time,a.system_status,a.system_rto,a.status_ap_user,a.status_ap_end_time,b.depend_name from brsinfo a left join (select a.system_id,group_concat(b.system_name) "depend_name" from systemdepend a join brsinfo b on a.depend=b.system_id and b.system_status < 3 group by a.system_id) b on a.system_id=b.system_id'
    list_obj = brsinfo.objects.raw(mainsql)
    #list_obj = brsinfo.objects.all()
    if list_obj:
        # 建立工作薄
        ws = Workbook(encoding='utf-8')
        w = ws.add_sheet(u"資料報表")
        w.write(0, 0, "ID")
        w.write(0, 1, u"系統名稱")
        w.write(0, 2, u"相依系統")
        w.write(0, 3, "SP")
        w.write(0, 4, u"系統開啟時間")
        w.write(0, 5, u"系統啟動完成時間")
        w.write(0, 6, u"證AP")
        w.write(0, 7, u"AP驗證完成時間")
        w.write(0, 8, u"驗證業管")
        w.write(0, 9, u"驗證完成時間")
        w.write(0, 10, u"系統RTO")
        # 寫入資料
        excel_row = 1
        for items in list_obj:
            data_id = items.system_id
            data_sysname = items.system_name
            if items.depend_name is None:
                data_depend = "-"
            else:
                data_depend = items.depend_name
            if items.status_infra_user is None:
                data_infra_user = "-"
            else:
                data_infra_user = items.status_infra_user
            if items.status_system_start_time is None:
                data_start_time = "-"
            else:
                data_start_time = datetime.strftime(items.status_system_start_time, '%Y/%m/%d %H:%M:%S')
            if items.status_system_end_time is None:
                data_end_time = "-"
            else:
                data_end_time = datetime.strftime(items.status_system_end_time, '%Y/%m/%d %H:%M:%S')
            if items.status_ap_user is None:
                data_ap_user = "-"
            else:
                data_ap_user = items.status_ap_user
            if items.status_ap_end_time is None:
                data_ap_time = "-"
            else:
                data_ap_time = datetime.strftime(items.status_ap_end_time, '%Y/%m/%d %H:%M:%S')
            if items.status_ba_user is None:
                data_ba_user = "-"
            else:
                data_ba_user = items.status_ba_user
            if items.status_check_end_time is None:
                data_ba_time = "-"
            else:
                data_ba_time = datetime.strftime(items.status_check_end_time, '%Y/%m/%d %H:%M:%S')
            if items.system_rto is None:
                data_rto = "-"
            else:
                data_rto = items.system_rto
            w.write(excel_row, 0, data_id)
            w.write(excel_row, 1, data_sysname)
            w.write(excel_row, 2, data_depend)
            w.write(excel_row, 3, data_infra_user)
            w.write(excel_row, 4, data_start_time)
            w.write(excel_row, 5, data_end_time)
            w.write(excel_row, 6, data_ap_user)
            w.write(excel_row, 7, data_ap_time)
            w.write(excel_row, 8, data_ba_user)
            w.write(excel_row, 9, data_ba_time)
            w.write(excel_row, 10, data_rto)
            excel_row += 1
        # 檢測檔案是夠存在
        # 方框中程式碼是儲存本地檔案使用，如不需要請刪除該程式碼
        ############################
        # exist_file = os.path.exists("result.xls")
        # if exist_file:
        #     os.remove(r"result.xls")
        # ws.save("result.xls")
        #############################
        sio = BytesIO()
        ws.save(sio)
        sio.seek(0)
        response = HttpResponse(sio.getvalue(), content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=brs_result.xls'
        response.write(sio.getvalue())
    return response
