from django.contrib import admin
from django.urls import path
from main.views import brsstatus, inputpage, update_status, reportpage
from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'brsstatus/', brsstatus.as_view(), name='brsstatus'),
    path(r'inputpage/', inputpage.as_view(), name='inputpage'),
    path(r'update/', update_status.as_view(), name='update'),
    path(r'reportpage/', reportpage.as_view(), name='reportpage'),
    path(r'exportpage/', views.exportpage, name='exportpage'),
]
