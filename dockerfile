FROM ubuntu:latest
MAINTAINER  David Mao "feverfree@gmail.com"
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install python3 python3-pip
RUN apt-get autoclean
RUN apt-get autoremove
RUN pip3 install django python-dateutil requests datetime xlwt
